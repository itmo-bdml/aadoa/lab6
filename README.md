## Lab 6

### Algorithms on graphs. Path search algorithms on weighted graphs

---
[Report (google doc)](https://docs.google.com/document/d/1c5RgGUEdwz9eBMysoYYhnppNgGzC8p9nnojbn1e2t3E)

Performed by:
* Elya Avdeeva
* Gleb Mikloshevich

### part one
Generated graph:

<img src="./lab/plots/graph.png" height="350">

We measured average time to calculate path to all vertices from the vertex 58.

* Dijkstra algorithm avg time: 0.0007

* Bellman-Ford algorithm avg time: 0.0016

### part two

We generated a 2d matrix (20, 10)

<img src="./lab/plots/map.png" height="350">

Then we implemented A* algorithm to calculate the path between two points

Path from (0, 1) to (19, 4):

<img src="./lab/plots/path_on_map.png" height="350">

Distance calculated by A* algorithm

<img src="./lab/plots/distance_map.png" height="350">

### part three

**Techniques:**
* Dynamic programming
* Greedy algorithm

**Data structures:**
* Graph
* Array
* Priority queue
