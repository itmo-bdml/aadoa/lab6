import time

import numpy as np
import matplotlib
import matplotlib.pyplot as plt
import networkx as nx

matplotlib.use('TkAgg')
np.random.seed(42)
VERTICES: int = 100
EDGES: int = 500
FIG_SIZE: tuple[int, int] = (16, 12)
MIN_WEIGHT: int = 1
MAX_WEIGHT: int = 50
REPEATS: int = 10


def generate_adjacency_matrix(vertices: int, edges: int, min_weight: int = 1, max_weight: int = 1) -> np.ndarray:
    """
    generates adjacency matrix for an unordered weighted graph with edges and vertices
    with integer weights from [min_weight, max_weight]
    :param vertices:
    :param edges:
    :param min_weight:
    :param max_weight:
    :return: a square adjacency matrix with
    """
    adjacency_matrix = np.zeros((vertices, vertices))
    i = 0
    while i < edges:
        v1, v2 = np.random.randint(low=0, high=vertices, size=2)
        if v1 == v2 or adjacency_matrix[v1, v2]:
            continue
        weight = np.random.randint(low=min_weight, high=max_weight, size=1)[0]
        adjacency_matrix[v1, v2] = weight
        adjacency_matrix[v2, v1] = weight
        i += 1
    return adjacency_matrix


adjacency_matrix = generate_adjacency_matrix(VERTICES, EDGES, MIN_WEIGHT, MAX_WEIGHT)
G = nx.from_numpy_array(adjacency_matrix)
pos = nx.drawing.spring_layout(G)
start, finish = np.random.randint(low=0, high=VERTICES, size=2)


# Dijkstra
dijkstra_time_list = []
for i in range(REPEATS):
    dijkstra_start_time = time.perf_counter()
    nx.single_source_dijkstra(G, start)
    dijkstra_time_list.append(time.perf_counter() - dijkstra_start_time)

# Bellman-Ford
bf_time_list = []
for i in range(REPEATS):
    bf_start_time = time.perf_counter()
    nx.single_source_bellman_ford(G, start)
    bf_time_list.append(time.perf_counter() - bf_start_time)

dijkstra_time = np.array(dijkstra_time_list).mean()
bf_time = np.array(bf_time_list).mean()
print(f"Dijkstra algorithm avg time: {np.round(dijkstra_time, 4)}")
print(f"Bellman-Ford algorithm avg time: {np.round(bf_time, 4)}")
plt.figure(figsize=FIG_SIZE)
nx.draw_networkx(G, font_size=8, pos=pos)
plt.savefig("./plots/graph.png")

