import time

import numpy as np
import matplotlib
import matplotlib.pyplot as plt
import queue
from collections.abc import Iterable

matplotlib.use('TkAgg')
np.random.seed(42)
X: int = 10
Y: int = 20
OBSTACLES: int = 40
FIG_SIZE = (16, 12)
ADJACENCY4_LIST = ((0, 1), (1, 0), (0, -1), (-1, 0))


def generate_adjacency_matrix(x: int, y: int, obstacles: int) -> np.ndarray:
    """
    :param x:
    :param y:
    :param obstacles:
    :return: obstacles_map: np.ndarray. Ones are obstacles
    """
    matrix = np.zeros((x, y))
    i = 0
    print(matrix.shape)
    while i < obstacles:
        v1 = np.random.randint(low=0, high=x, size=1)[0]
        v2 = np.random.randint(low=0, high=y, size=1)[0]
        if matrix[v1, v2]:
            continue
        matrix[v1, v2] = 1
        i += 1
    return matrix


matrx = generate_adjacency_matrix(X, Y, OBSTACLES)

# plt.figure(figsize=FIG_SIZE)
plt.matshow(matrx, cmap="Pastel1")
plt.title("Map. White cells are obstacles")
plt.savefig("./plots/map.png")
# plt.show()


def manhattan_distance(x1: int, y1: int, x2: int, y2: int) -> int:
    return abs(x1-x2) + abs(y1-y2)


def astar(start: Iterable, finish: Iterable, mp: np.ndarray,
          distance_func: callable = manhattan_distance, draw_path: bool = False, draw_distance_map: bool = False) -> np.ndarray:
    """
    generates a path using A* algorithm on a matrix from start to finish
    :param start: [x_start, y_start]
    :param finish: [x_finish, y_finish]
    :param mp: initial map where 0's are empty cells and 1's are obstacles
    :param distance_func: if you want to use custom matrix. It may work incorrect because weight between cells is
    hardcoded in the path restoring algorithm
    :param draw_path: plots and saves path on matrix
    :param draw_distance_map: plots and saves distance map matrix
    :return: np.ndarray of path cells
    """
    priority = queue.PriorityQueue(maxsize=200)
    visited = np.zeros_like(mp)
    priority.put((0, 1, start))
    while priority:
        _, weight, cur_node = priority.get()
        if visited[cur_node] > 0:
            continue
        visited[cur_node] = weight
        if cur_node[1] == finish[0] and cur_node[0] == finish[1]:
            break

        for adjacency in ADJACENCY4_LIST:
            new_x = cur_node[0] + adjacency[0]
            new_y = cur_node[1] + adjacency[1]
            if (0 <= new_x < visited.shape[0] and 0 <= new_y < visited.shape[1]
                    and not visited[new_x, new_y] and not mp[new_x, new_y]):
                priority.put(((weight+1 + distance_func(finish[0], new_x, finish[1], new_y),
                               weight+1,
                               (new_x, new_y))))

    path = [finish]
    i = 0
    while True:
        i += 1
        if i > 20:
            break
        for adjacency in ADJACENCY4_LIST:
            # here new_x, new_y means a next cell on the path from target to the start
            prev_x = path[-1][0]
            prev_y = path[-1][1]
            new_x = path[-1][0] + adjacency[0]
            new_y = path[-1][1] + adjacency[1]
            if (0 <= new_y < visited.shape[0] and 0 <= new_x < visited.shape[1]
                    and visited[prev_y, prev_x] == 1 + visited[new_y, new_x]):
                path.append((new_x, new_y))

        if path[-1][0] == start[0] and path[-1][1] == start[1]:
            print("finish")
            break
    if draw_distance_map:
        plt.matshow(visited, cmap="magma")
        plt.title("distance map")
        plt.colorbar()
        plt.savefig("./plots/distance_map.png")

    if draw_path:
        path_map = mp.copy()
        for y, x in path:
            path_map[x, y] = 0.5
        plt.matshow(path_map, cmap="Pastel1")
        plt.title("path on map")
        plt.savefig("./plots/path_on_map.png")
        plt.show()
    return np.array(path)


i = 0
time_list = []
while i < 5:
    t_start = time.perf_counter()
    x1, x2 = np.random.randint(low=0, high=9, size=2)
    y1, y2 = np.random.randint(low=0, high=19, size=2)
    if matrx[x1, y1] or matrx[x2, y2]:
        continue
    i += 1
    a = astar((0, 1), (19, 4), matrx, draw_path=False, draw_distance_map=False)
    time_list.append(time.perf_counter() - t_start)
print(f"avg execution time: {np.round(np.array(time_list).mean(), 5)}")
